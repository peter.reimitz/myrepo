#problem 14
def collatz (n):
    if n%2==0:
        return n/2
    return 3*n+1


def collatz_list(n):
    clist=[n]
    while clist[-1] != 1:
        clist.append(collatz(clist[-1]))
    return clist

def collatz_size(n):
    size=0
    last_element= n
    while last_element != 1:
        last_element= collatz(last_element)
        size+=1
    return size

max_size=1
max_startnumber=1

#for i in range(1,1000000):
#    if len(collatz_list(i)) > max_size:
#        max_size=len(collatz_list(i))
#        max_startnumber=i

for i in range(1,1000000):
    if collatz_size(i) > max_size:
        max_size=collatz_size(i)
        max_startnumber=i

print max_startnumber
