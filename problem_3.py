def primes(n):
    prim_factors=[]
    d=2
    while d*d<= n:
        while n % d ==0:
            prim_factors.append(d)
            n //= d
        d+=1
    if n > 1:
        prim_factors.append(n)
    return prim_factors
m=13195
print("largest prime number of ",m , "is ", primes(m)[-1])
